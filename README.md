#Instructions Globales
===========================

##Git
-------

Merci de respecter le **gitignore** pour pas commit n'importe quoi.  
Ne pas commit sa *scene* parce que pour pull c'est toujours le bordel. 
Mettre des __messages de commits cohérents__ pour pas qu'on s'y perde.  
Après chaque sprint il faudra créer une __nouvelle branche__ au projet, histoire de versionner.  

##Trello
----------

Faire correspondre les tâches aux sprints.  
Pour ce-faire un code couleur par sprint va être imposé.  
Sprint 1 -> Jaune.  
Sprint 2 -> Bleu.  
Sprint 3 -> Vert.  
Si une tâche n'est pas finie ou a besoin de correction, faire suivre dans le tableau.  
Un sprint ~= 1 mois.  

